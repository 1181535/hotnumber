package hotnumber;
import java.util.Scanner;
import java.util.Random;

public class HotNumber {
    public static void main(String[] args) {
        int nr = 0, nr_user = 0, diff = 0, tentativa = 0, users = 0;

        Scanner scan = new Scanner(System.in);
        Random rand = new Random();

        nr = rand.nextInt(100) + 1; //100 nº máx, 1 nº min

        System.out.println("Número de tentativas:");
        tentativa = scan.nextInt();

        System.out.println("Número de utilizadores:");
        users = scan.nextInt();

        for (int i = 0; i < tentativa; i++) {
            for (int j = 1; j < users +1; j++) {
                System.out.println("Jogador "+ j +". Insira um número:");
                nr_user = scan.nextInt();

                diff = nr - nr_user; //calcula a diferença

                if (diff != 0) {

                    if (diff > 25) {
                        System.out.println("Frio!");
                    } else if (diff <= 25 && diff >= 5) {
                        System.out.println("Morno!");

                    } else if (diff < 5) {
                        System.out.println("Quente!");
                    }

                } else {
                    System.out.println("Parabéns jogador, acertou!");
                    i = tentativa;

                }
            }
        }
        System.out.println("Acabaram as tentativas!");
    }
}
